package org.fallingfruit.ffruit.db;

import static org.junit.Assert.*;

import com.google.common.collect.ImmutableMap;

import android.content.ContentValues;
import android.os.Build;

import org.fallingfruit.ffruit.db.FFruitContract;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.robolectric.annotation.Config;
import org.robolectric.bazel.BazelRobolectricTestRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RunWith(BazelRobolectricTestRunner.class)
@Config(sdk = Build.VERSION_CODES.LOLLIPOP, manifest = Config.NONE)
public class FFruitContractTest {

  @Test
  public void testCreateTypesTablesStatement() {
    String expectedSql = new StringBuilder()
      .append("CREATE TABLE types\n")
      .append("(\n")
      .append("_id INTEGER PRIMARY KEY,\n")
      .append("checksum TEXT,\n")
      .append("parent_id TEXT,\n")
      .append("name TEXT,\n")
      .append("synonyms TEXT,\n")
      .append("es_name TEXT,\n")
      .append("he_name TEXT,\n")
      .append("pt_br_name TEXT,\n")
      .append("fr_name TEXT,\n")
      .append("de_name TEXT,\n")
      .append("pl_name TEXT,\n")
      .append("scientific_name TEXT,\n")
      .append("scientific_synonyms TEXT,\n")
      .append("taxonomic_rank TEXT,\n")
      .append("category_mask TEXT\n")
      .append(")")
      .toString();
    assertEquals(expectedSql, FFruitContract.Types.createTableStmt());
  }

  @Test
  public void testFromMapToContentValues() {
    String expectedChecksum = "5a98e68ea86bf952ac90fe89f8cb8298510d97e0bf8b8d0fc41614600cb458e7";
    Map<String, String> map = ImmutableMap.of("foo", "bar", "baz", "bux");
    ContentValues actual = FFruitContract.fromMap(map);
    for (Map.Entry<String, String> pair : map.entrySet()) {
      assertEquals(pair.getValue(), actual.getAsString(pair.getKey()));
    }
    assertEquals(expectedChecksum, actual.getAsString(FFruitContract.BaseColumns.COL_CHKSUM));
  }

  @Test
  public void testInsertIntoStmt() {
    String expectedSql = new StringBuilder()
      .append("INSERT INTO foo ")
      .append("(_id, checksum, one, two) ")
      .append("VALUES (\"id_a\", ")
      .append("\"b8d73a53afade31a7602bc7c49e0c4701596c2281f406e79512f41c3cc60044c\", ")
      .append("\"one_val_a\", \"two_val_a\"), ")
      .append("(\"id_b\", ")
      .append("\"f23c61cec7aefb5f2e0f36afb251bff3befb5f5f71b921cd43111784677a7c4b\", ")
      .append("\"one_val_b\", \"two_val_b\");")
      .toString();
    List<ContentValues> vals = Arrays.asList(
      FFruitContract.fromMap(
        ImmutableMap.of("id", "id_a", "one", "one_val_a", "two", "two_val_a")),
      FFruitContract.fromMap(
        ImmutableMap.of("id", "id_b", "one", "one_val_b", "two", "two_val_b"))
    );
    List<String> cols = Arrays.asList(
      FFruitContract.BaseColumns._ID,
      FFruitContract.BaseColumns.COL_CHKSUM,
      "one",
      "two"
    );
    assertEquals(expectedSql, FFruitContract.insertIntoStmt("foo", cols, vals));
  }

}
