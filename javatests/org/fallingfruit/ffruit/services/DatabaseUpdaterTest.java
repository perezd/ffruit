package org.fallingfruit.ffruit.services;

import static org.junit.Assert.*;

import android.os.Build;

import org.junit.runner.RunWith;

import org.robolectric.annotation.Config;
import org.robolectric.bazel.BazelRobolectricTestRunner;

@RunWith(BazelRobolectricTestRunner.class)
@Config(sdk = Build.VERSION_CODES.LOLLIPOP,
        manifest = Config.NONE)
public class DatabaseUpdaterTest {
}
