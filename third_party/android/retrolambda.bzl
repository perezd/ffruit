# retrolambda_java_library makes it possible to utilize
# JDK8 features (such as lambdas) in non-JDK8 environments (like Android).
def retrolambda_java_library(name, srcs, verbose=0):
    bin_name = name + "_bin"
    import_name = name + "_import"
    classpath_name = name + "_classpath"
    retrolambda_name = name+"_retrolambda"

    # We assume that the Android SDK needs to be present in our
    # Retrolambda classpath, so we make a stub jar here.
    native.java_binary(
        name = "___android_sdk",
        main_class = "do.not.want",
        runtime_deps = [
            "//external:android_sdk",
        ]
    )

    # Next we create a "singlejar" of all the sources
    # we intend to run through Retrolambda and make it
    # a useable target for bazel rules.
    native.java_binary(
        name = bin_name,
        runtime_deps = srcs,
    )
    native.java_import(
        name = "pre_" + import_name,
        jars = [":" + bin_name + "_deploy.jar"],
    )

    # Populate the classpath for retrolambda to reflect
    # all of the inputs we want to pass through for
    # transformation.
    native.genrule(
        name = classpath_name,
        srcs = [
            ":pre_"+import_name,
        ],
        tools = [
            ":___android_sdk_deploy.jar",
        ],
        cmd = """
        echo $(location :___android_sdk_deploy.jar) >> $@; \
        echo $< >> $@""",
        outs = ["classfiles.txt"],
    )

    # Do it!
    native.genrule(
        name = retrolambda_name,
        message = "Reticulating splices with Retrolambda...",
        local = 1,
        output_to_bindir = 1,
        srcs = [
            ":pre_" + import_name,
            ":" + classpath_name,
            ":___android_sdk_deploy.jar",
        ],
        tools = [
            "@retrolambda//jar",
        ],
        cmd = """
        TMPDIR=$$(mktemp -d)
        mkdir -p $$TMPDIR; \
        CWD=$$(pwd)
        (cd $$TMPDIR; \
        /usr/bin/jar xf $$CWD/$(location {injar})); \

        /usr/bin/java \
        -Dretrolambda.inputDir=$$TMPDIR \
        -Dretrolambda.classpathFile=$(location {classpath}) \
        -javaagent:$(location @retrolambda//jar) \
        -jar $(location @retrolambda//jar) {output}; \

        (cd $$TMPDIR;
        /usr/bin/jar cf classes.jar .);
        cp $$TMPDIR/classes.jar $@
        rm -rf $$TMPDIR;
        """
        .format(
            classpath=":" + classpath_name,
            injar=":pre_" + import_name,
            android=":___android_sdk_deploy.jar",
            output= "> /dev/null" if verbose == 0 else "",
        ),
        outs = ["classes.jar"],
    )

    # Lastly, we expose the classes.jar file we produced
    # as a single artifact to depend on.
    native.java_import(
        name = name,
        jars = [":" + retrolambda_name],
    )
