def android():
    native.android_sdk_repository(
        name="android_sdk_config",
        path="sdk/android",
        api_level=22,
        build_tools_version="23.0.2"
    )
    native.bind(
        name = "android_sdk_jar",
        actual = "@android_sdk_config//:platforms/android-22/android.jar",
    )
    native.bind(
        name = "android_sdk",
        actual = "//third_party/android:sdk",
    )
    native.http_jar(
        name = "retrolambda",
        url = "https://oss.sonatype.org/content/groups/public/net/orfjackal/retrolambda/retrolambda/2.1.0/retrolambda-2.1.0.jar",
        sha256 = "50998686780f8782ff469b26d873f7d5d58790a81b58b31ea4870aa065aa7c02",
    )
