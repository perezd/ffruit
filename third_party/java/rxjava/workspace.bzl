
def rxjava():
    native.maven_jar(
        name = "rxjava",
        artifact = "io.reactivex:rxjava:1.1.0",
    )

    native.new_http_archive(
        name = "rxjava_android",
        url = "https://repo1.maven.org/maven2/io/reactivex/rxandroid/1.1.0/rxandroid-1.1.0.aar",
        sha256 = "f0d803d002e2e58a718a476d7efcb9b5b547fe6125b3cc07c39de545ada5c217",
        type = "zip",
        build_file = "third_party/java/rxjava/rxjava_android.BUILD",
    )
