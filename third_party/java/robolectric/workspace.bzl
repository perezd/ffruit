
def robolectric():
    # NOTE: make sure this is in sync with //third_party/android.
    native.maven_jar(
        name = "org_robolectric_android_sdk",
        artifact = "org.robolectric:android-all:5.0.0_r2-robolectric-1",
    )
    native.maven_jar(
        name = "org_robolectric",
        artifact = "org.robolectric:robolectric:3.0",
    )
    native.maven_jar(
        name = "org_ow2_asm_all",
        artifact = "org.ow2.asm:asm-all:5.0.4",
    )
    native.maven_jar(
        name = "org_robolectric_shadows_core",
        artifact = "org.robolectric:shadows-core:3.0",
    )
    native.maven_jar(
        name = "org_robolectric_robolectric_utils",
        artifact = "org.robolectric:robolectric-utils:3.0",
    )
    native.maven_jar(
        name = "org_robolectric_robolectric_resources",
        artifact = "org.robolectric:robolectric-resources:3.0",
    )
    native.maven_jar(
        name = "org_json_json",
        artifact = "org.json:json:20080701",
    )
    native.maven_jar(
        name = "com_almworks_sqlite4java_sqlite4java",
        artifact = "com.almworks.sqlite4java:sqlite4java:0.282",
    )
    native.maven_jar(
        name = "org_ccil_cowan_tagsoup_tagsoup",
        artifact = "org.ccil.cowan.tagsoup:tagsoup:1.2",
    )
    native.maven_jar(
        name = "org_robolectric_robolectric_annotations",
        artifact = "org.robolectric:robolectric-annotations:3.0",
    )
    native.maven_jar(
        name = "org_bouncycastle_bcprov",
        artifact = "org.bouncycastle:bcprov-jdk16:1.46",
    )
