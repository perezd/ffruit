package org.robolectric.bazel;

import org.junit.runners.model.InitializationError;

import org.robolectric.RobolectricTestRunner;
import org.robolectric.internal.dependency.DependencyResolver;
import org.robolectric.internal.dependency.DependencyJar;

import java.io.File;
import java.io.IOException;

import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;

import java.net.URL;
import java.net.URLClassLoader;

import java.util.HashMap;

/**
 * Provides a Bazel friendly jar resolver that maps Robolectric's runtime jar needs
 * to bazel's "runfiles" filesystem. This test runner works completely offline.
 */
public final class BazelRobolectricTestRunner extends RobolectricTestRunner {

  private static final class BazelDependencyResolver implements DependencyResolver {

    private final HashMap<String, URL> jars = new HashMap<>();

    /**
     * Bazel stores jars in a named hierarchy. All that needs to happen is to walk
     * the tree looking for jar paths, so that we can convert them to URLs.
     */
    BazelDependencyResolver(File root) {
      try {
        Files.walkFileTree(root.toPath(), new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
              throws IOException {
              Path fullPath = file.toAbsolutePath();
              if (fullPath.toString().endsWith(".jar")) {
                jars.put(fullPath.getFileName().toString(), fullPath.toUri().toURL());
              }
              return FileVisitResult.CONTINUE;
            }
          });
      } catch (IOException ex) {
        throw new IllegalStateException(ex);
      }
    }

    @Override
    public URL[] getLocalArtifactUrls(DependencyJar... dependencies) {
      URL[] urls = new URL[dependencies.length];
      for (int i=0; i < dependencies.length; i++) {
        urls[i] = getLocalArtifactUrl(dependencies[i]);
      }
      return urls;
    }

    @Override
    public URL getLocalArtifactUrl(DependencyJar dependency) {
      String slug = String.format("%s-%s.jar",
                                  dependency.getArtifactId(), dependency.getVersion());
      return jars.get(slug);
    }
  }

  private final DependencyResolver resolver;

  public BazelRobolectricTestRunner(final Class<?> testClass) throws InitializationError {
    super(testClass);
    this.resolver = new BazelDependencyResolver(new File(System.getenv("JAVA_RUNFILES")));
  }

  @Override
  protected DependencyResolver getJarResolver() {
    return resolver;
  }
}
