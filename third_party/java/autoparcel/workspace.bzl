
def autoparcel():
    native.maven_jar(
        name = "autocommon",
        artifact = "com.google.auto:auto-common:0.5",
    )
    native.maven_jar(
        name = "autoparcel",
        artifact = "com.github.frankiesardo:auto-parcel:0.3.1",
    )
    native.maven_jar(
        name = "autoparcel_processor",
        artifact = "com.github.frankiesardo:auto-parcel-processor:0.3.1",
    )
    native.maven_jar(
        name = "autoservice",
        artifact = "com.google.auto.service:auto-service:1.0-rc2",
    )
    native.maven_jar(
        name = "velocity",
        artifact = "org.apache.velocity:velocity:1.7",
    )
    native.maven_jar(
        name = "commons_lang",
        artifact = "commons-lang:commons-lang:2.6",
    )
    native.maven_jar(
        name = "commons_collections",
        artifact = "commons-collections:commons-collections:3.2.2",
    )
