#!/bin/bash

# Fetch Bazel and install it.
BUILD_BASE=${PWD}/build
if [ ! -d $BUILD_BASE ]
then
    BAZEL_VERSION=0.2.0
    INSTALLER_PLATFORM=linux-x86_64
    mkdir -p $BUILD_BASE
    if [ -n "$FROM_SOURCE" ];
    then
        mkdir -p $BUILD_BASE/src
        git clone https://github.com/bazelbuild/bazel.git $BUILD_BASE/src
        $BUILD_BASE/src/compile.sh
        mkdir -p $BUILD_BASE/bin
        mv $BUILD_BASE/src/output/bazel $BUILD_BASE/bin/bazel
    else
        BAZEL_INSTALLER_URL=https://github.com/bazelbuild/bazel/releases/download/${BAZEL_VERSION}/bazel-${BAZEL_VERSION}-installer-${INSTALLER_PLATFORM}.sh
        export BAZEL_INSTALLER=${BUILD_BASE}/install.sh
        curl -L -o ${BAZEL_INSTALLER} ${BAZEL_INSTALLER_URL}
        bash "${BAZEL_INSTALLER}" \
             --base="${BUILD_BASE}" \
             --bazelrc="${BUILD_BASE}/etc/bazel.bazelrc" \
             --bin="${BUILD_BASE}/binary"
    fi
fi

# Fetch Android SDK and set it up.
SDK_BASE=${PWD}/sdk
if [ ! -d $SDK_BASE ]
then
    mkdir -p $SDK_BASE
    SDK_FEATURES=platform-tools,extra-android-support,android-22,android-21,build-tools-23.0.2
    SDK_INSTALLER=${SDK_BASE}/sdk.tgz
    ANDROID_SDK_URL=http://dl.google.com/android/android-sdk_r24.4.1-linux.tgz

    wget -O ${SDK_INSTALLER} ${ANDROID_SDK_URL}
    tar -zxf ${SDK_INSTALLER} -C $SDK_BASE/
    mv $SDK_BASE/android-sdk-linux $SDK_BASE/android

    # Download the necessary SDKs for Android.
    ( sleep 1 && while [ 1 ]; do sleep 1; echo y; done ) |
        $SDK_BASE/android/tools/android update sdk -u -a -t $SDK_FEATURES
fi

# Verify we can build that main binary.
$BUILD_BASE/bin/bazel clean
$BUILD_BASE/bin/bazel build //java/org/fallingfruit/ffruit
