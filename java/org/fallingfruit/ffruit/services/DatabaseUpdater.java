package org.fallingfruit.ffruit.services;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import android.content.ContentValues;
import android.content.Context;

import android.util.Log;

import org.fallingfruit.ffruit.db.FFruitContract;
import org.fallingfruit.ffruit.util.observables.CsvRowEmitterObservable;
import org.fallingfruit.ffruit.util.observables.CsvRowEmitterObservable.Row;
import org.fallingfruit.ffruit.util.observables.FetchBz2Observable;
import org.fallingfruit.ffruit.util.observables.FetchHeadersObservable;

import rx.Observable;

import java.util.ArrayList;
import java.util.List;


final class DatabaseUpdater {

  private static final String TAG = "DatabaseUpdater";
  private static final String LOCATIONS_ENDPOINT = "https://storage.googleapis.com/ffruit/locations.csv.bz2";
  private static final String TYPES_ENDPOINT = "https://storage.googleapis.com/ffruit/types.csv.bz2";
  private static final String ENDPOINT_DIGEST_HEADER = "ETag";

  private static final int MAX_CSV_ENTIRES_PER_WINDOW = 100;
  private static final int MAX_UPDATE_CHECK_RETRIES = 3;

  public static void run(Context context) {
    final DatabaseUpdater updater = new DatabaseUpdater(context);
    updater
      .checkForUpdate()
      .subscribe(updater::maybeUpdate);
  }

  private final Context context;

  private DatabaseUpdater(Context context) {
    this.context = context;
  }

  Observable<Boolean> checkForUpdate() {
    return Observable.merge(FetchHeadersObservable.create(context,
                                                          ImmutableList.of(ENDPOINT_DIGEST_HEADER),
                                                          TYPES_ENDPOINT),
                            FetchHeadersObservable.create(context,
                                                          ImmutableList.of(ENDPOINT_DIGEST_HEADER),
                                                          LOCATIONS_ENDPOINT))
      .retry(MAX_UPDATE_CHECK_RETRIES)
      .map(
        response -> {
          // With this map, we check that for every endpoint we've seen, that
          // the newSum provided by the HTTP request is not the same as the old one
          // we've seen before.
          String newSum = response.headers().get(ENDPOINT_DIGEST_HEADER);
          String oldSum = ServicePrefs
            .getEndpointDigest(DatabaseUpdater.this.context, response.endpoint());
          return newSum != oldSum;
        }
      )
      .contains(true);
  }

  void update() {
    Log.i(TAG, "Database update requested.");
    Observable.merge(FetchBz2Observable.create(context, TYPES_ENDPOINT),
                     FetchBz2Observable.create(context, LOCATIONS_ENDPOINT))
      // Convert the above BufferedReaders into CsvRowEmitters. This will
      // provide everyone below with a burst of MAX_CSV_ENTRIES_PER_WINDOW
      // of Row instances. These will be unordered and could be from any
      // of the above sources.
      .flatMap(r -> CsvRowEmitterObservable.create(r.reader(), r.endpoint()))
      .window(MAX_CSV_ENTIRES_PER_WINDOW)
      // Since we receive arbitrary windows of csv Row instances, in order to
      // enable statements below to better make sense of what they have,
      // we transform the window into a map where the key is Row::id,
      // and the values are transformed into a list of ContentValues.
      .flatMap(window -> window.groupBy(Row::id))
      .flatMap(
        group -> {
          return group
            .collect(ArrayList<ContentValues>::new, (acc, row) ->
                     acc.add(FFruitContract.fromMap(row.values())))
            .map(rows -> ImmutableMap.of(group.getKey(), rows));
        }
      )
      .forEach(
        chunk -> {
          for (String key : chunk.keySet()) {
            List<ContentValues> vals = chunk.get(key);
            for (ContentValues val : vals) {
              Log.d(TAG, val.toString());
            }
          }
        }
      );
  }

  private void maybeUpdate(boolean canProceed) {
    if (canProceed) {
      update();
    }
  }
}
