package org.fallingfruit.ffruit.services;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Helpful wrapper for common puts/gets that the services package classes
 * will utilize.
 */
final class ServicePrefs {

  private static final String PREF_KEY = "org.fallingfruit.ffruit.services.PREFS";

  static SharedPreferences get(Context context) {
    return context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
  }

  static String getEndpointDigest(Context context, String endpoint) {
    return get(context).getString(String.format("ENDPOINT_%s", endpoint), "--MISSING--");
  }

  static boolean putEndpointDigest(Context context, String endpoint, String digest) {
    return get(context)
      .edit()
      .putString(String.format("ENDPOINT_%s", endpoint), digest)
      .commit();
  }
}
