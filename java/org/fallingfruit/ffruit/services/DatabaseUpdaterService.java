package org.fallingfruit.ffruit.services;

import android.app.IntentService;

import android.content.Intent;

import android.util.Log;

import org.fallingfruit.ffruit.util.observables.ChargingStatusObservable;
import org.fallingfruit.ffruit.util.observables.WifiStatusObservable;

import rx.Observable;

/**
 * Fallingfruit.org updates its database dumps daily, but they can be
 * very large, so in order to be a good Android citizen, we do not attempt
 * to update the database unless we can confirm that the device is on WiFi
 * and currently plugged in.
 */
public final class DatabaseUpdaterService extends IntentService {

  private static final String TAG = "DatabaseUpdaterService";

  public DatabaseUpdaterService() {
    super(TAG);
  }

  @Override
  public void onHandleIntent(Intent intent) {
    Log.d(TAG, "Intent received.");
    // Before we attempt to update the database, we should verify that
    // the device is currently on WiFi, and its also plugged in.
    Observable<Boolean> prereqs = Observable.merge(WifiStatusObservable.create(this),
                                                   ChargingStatusObservable.create(this));
    prereqs
      .all(check -> check)
      .subscribe(
        canProceed -> {
          if (canProceed) {
            Log.d(TAG, "Preconditions cleared, starting updater.");
            DatabaseUpdater.run(DatabaseUpdaterService.this);
          }
        }
      );
  }

}
