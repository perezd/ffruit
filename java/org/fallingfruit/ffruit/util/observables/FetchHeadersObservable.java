package org.fallingfruit.ffruit.util.observables;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import android.content.Context;

import auto.parcel.AutoParcel;

import okhttp3.Request;
import okhttp3.Response;

import rx.Observable;

import java.util.List;
import java.util.Map;

/**
 * Returns a map of header/value pairs for the provided list of headers and endpoint.
 */
public final class FetchHeadersObservable
  extends HttpRequestObservable<FetchHeadersObservable.Response> {

  @AutoParcel
  public abstract static class Response {
    public abstract String endpoint();
    public abstract Map<String, String> headers();
  }

  public static Observable<FetchHeadersObservable.Response> create(Context context,
                                           List<String> headers,
                                           String endpoint) {
    return Observable.create(new FetchHeadersObservable(context, headers, endpoint));
  }

  private final ImmutableList<String> headers;
  private final String endpoint;

  private FetchHeadersObservable(Context context,
                                List<String> headers,
                                String endpoint) {
    super(context);
    this.headers = ImmutableList.copyOf(headers);
    this.endpoint = endpoint;
  }

  @Override
  public Request buildRequest(Request.Builder builder) {
    return builder
      .head()
      .url(endpoint)
      .build();
  }

  @Override
  public FetchHeadersObservable.Response onResponse(okhttp3.Response response) {
    ImmutableMap.Builder<String, String> outHeaders = new ImmutableMap.Builder<>();
    for (String header : headers) {
      outHeaders.put(header, response.header(header));
    }
    return new AutoParcel_FetchHeadersObservable_Response(endpoint, outHeaders.build());
  }
}
