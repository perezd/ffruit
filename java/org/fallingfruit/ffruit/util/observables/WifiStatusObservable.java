package org.fallingfruit.ffruit.util.observables;

import android.content.Context;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import rx.Observable.OnSubscribe;
import rx.Observable;
import rx.Subscriber;

/** Checks if the device is actively using WiFi. */
public final class WifiStatusObservable implements OnSubscribe<Boolean> {

  public static Observable create(Context context) {
    return Observable.create(new WifiStatusObservable(context));
  }

  private final Context context;

  private WifiStatusObservable(Context context) {
    this.context = context;
  }

  @Override
  public void call(Subscriber<? super Boolean> sub) {
    ConnectivityManager manager = (ConnectivityManager) context
      .getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetwork = manager.getActiveNetworkInfo();
    if (sub.isUnsubscribed()) {
      return;
    }
    sub.onNext(isWifi(activeNetwork) && isConnected(activeNetwork));
    sub.onCompleted();
  }

  private static boolean isWifi(NetworkInfo network) {
    return network != null && network.getType() == ConnectivityManager.TYPE_WIFI;
  }

  private static boolean isConnected(NetworkInfo network) {
    return network != null && network.isConnectedOrConnecting();
  }

}
