package org.fallingfruit.ffruit.util.observables;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

import rx.Observable.OnSubscribe;
import rx.Observable;
import rx.Subscriber;

/** Checks if the device is actively charging. */
public final class ChargingStatusObservable implements OnSubscribe<Boolean> {

  public static Observable create(Context context) {
    return Observable.create(new ChargingStatusObservable(context));
  }

  private final Context context;

  private ChargingStatusObservable(Context context) {
    this.context = context;
  }

  @Override
  public void call(Subscriber<? super Boolean> sub) {
    IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
    Intent batteryStatus = context.registerReceiver(null, ifilter);
    int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
    boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING
      || status == BatteryManager.BATTERY_STATUS_FULL;
    if (sub.isUnsubscribed()) {
      return;
    }
    sub.onNext(isCharging);
    sub.onCompleted();
  }

}
