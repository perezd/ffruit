package org.fallingfruit.ffruit.util.observables;

import android.content.Context;

import auto.parcel.AutoParcel;

import okhttp3.Request;
import okhttp3.Response;

import org.fallingfruit.ffruit.util.sources.BZip2Source;

import rx.Observable;

import java.io.BufferedReader;

/**
 * Fetches and decompresses a Bzip2 file found at the provided endpoint.
 */
public final class FetchBz2Observable extends HttpRequestObservable<FetchBz2Observable.Response> {

  @AutoParcel
  public abstract static class Response {
    public abstract String endpoint();
    public abstract BufferedReader reader();
  }

  public static Observable<FetchBz2Observable.Response> create(Context context, String endpoint) {
    return Observable.create(new FetchBz2Observable(context, endpoint));
  }

  private final String endpoint;

  private FetchBz2Observable(Context context, String endpoint) {
    super(context);
    this.endpoint = endpoint;
  }

  @Override
  public Request buildRequest(Request.Builder builder) {
    return builder
      .get()
      .url(endpoint)
      .build();
  }

  @Override
  public FetchBz2Observable.Response onResponse(okhttp3.Response response) {
    BufferedReader reader = BZip2Source.toBufferedReader(response.body().source());
    return new AutoParcel_FetchBz2Observable_Response(endpoint, reader);
  }
}
