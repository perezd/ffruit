package org.fallingfruit.ffruit.util.observables;

import android.content.Context;
import android.util.Log;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor.Level;
import okhttp3.logging.HttpLoggingInterceptor.Logger;
import okhttp3.logging.HttpLoggingInterceptor;

import rx.Observable.OnSubscribe;
import rx.Subscriber;

import java.io.IOException;

/**
 * HttpRequestObservable automates the mechanics of making an HTTP Request
 * with okhttp3, and maps the transaction lifecycle to an RxJava Observable.
 */
public abstract class HttpRequestObservable<T> implements OnSubscribe<T> {

  private static final String TAG = "HttpRequestObservable";

  private final Context context;
  private final OkHttpClient httpClient;

  public HttpRequestObservable(Context context) {
    this(context, Level.NONE);
  }

  public HttpRequestObservable(Context context, Level logLevel) {
    this.context = context;
    this.httpClient = newHttpClient(logLevel);
  }

  /** Called before the request in enqueued for processing. */
  public abstract Request buildRequest(Request.Builder builder);

  /** Called after the enqueued request has fully transacted. */
  public abstract T onResponse(Response response);

  @Override
  public void call(final Subscriber<? super T> sub) {
    final Request request = buildRequest(new Request.Builder());
    Log.d(TAG, String.format("Initiating %s", request));
    httpClient
      .newCall(request)
      .enqueue(new Callback() {
          @Override
          public void onFailure(Call call, IOException ex) {
            Log.e(TAG, String.format("Failed %s", request));
            if (sub.isUnsubscribed()) {
              return;
            }
            sub.onError(ex);
          }
          @Override
          public void onResponse(Call call, Response response) {
            Log.d(TAG, String.format("Succeeded %s", request));
            if (sub.isUnsubscribed()) {
              return;
            }
            sub.onNext(HttpRequestObservable.this.onResponse(response));
            sub.onCompleted();
          }
        });
  }

  protected Context context() {
    return context;
  }

  protected OkHttpClient client() {
    return httpClient;
  }

  private OkHttpClient newHttpClient(Level logLevel) {
    OkHttpClient.Builder builder = new OkHttpClient.Builder();
    if (logLevel != Level.NONE) {
      HttpLoggingInterceptor logging = new HttpLoggingInterceptor(new Logger() {
          @Override
          public void log(String message) {
            Log.d(TAG, message);
          }
        });
      logging.setLevel(logLevel);
      builder.addInterceptor(logging);
    }
    return builder.build();
  }
}
