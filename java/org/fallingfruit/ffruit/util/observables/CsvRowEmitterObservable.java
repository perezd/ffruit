package org.fallingfruit.ffruit.util.observables;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.Closeables;

import auto.parcel.AutoParcel;

import com.opencsv.CSVReader;

import rx.Observable;
import rx.Observable.OnSubscribe;
import rx.Subscriber;

import java.io.BufferedReader;
import java.util.List;
import java.util.Map;

/**
 * Emits a {@link CsvRowEmitterObservable.Row} for each line of the provided
 * {@code BufferedReader} of CSV data.
 */
public final class CsvRowEmitterObservable
  implements OnSubscribe<CsvRowEmitterObservable.Row> {

  @AutoParcel
  public abstract static class Row {
    public abstract String id();
    public abstract Map<String, String> values();

    @Override
    public String toString() {
      return String.format("(%s): %s",
                           id(),
                           Joiner.on(",")
                           .withKeyValueSeparator("=")
                           .join(values()));
    }
  }

  public static Observable<Row> create(BufferedReader reader, String id) {
    return Observable.create(new CsvRowEmitterObservable(reader, id));
  }

  private final CSVReader reader;
  private final String id;
  private List<String> columns;

  private CsvRowEmitterObservable(BufferedReader reader, String id) {
    this.reader = new CSVReader(reader);
    this.id = id;
  }

  @Override
  public void call(Subscriber<? super Row> sub) {
    try {
      String[] row;
      while (!sub.isUnsubscribed()
             && (row = reader.readNext()) != null) {
        if (columns == null) {
          columns = ImmutableList.copyOf(row);
          continue;
        }
        sub.onNext(emitRow(row, columns, id));
      }
      Closeables.close(reader, true);
      sub.onCompleted();
    } catch (Exception ex) {
      sub.onError(ex);
    }
  }

  private static Row emitRow(String[] row, List<String> columns, String id) {
    ImmutableMap.Builder<String, String> values = new ImmutableMap.Builder<>();
    for (int i = 0; i < row.length; i++) {
      values.put(columns.get(i), row[i]);
    }
    return new AutoParcel_CsvRowEmitterObservable_Row(id, values.build());
  }
}
