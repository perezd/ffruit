package org.fallingfruit.ffruit.util.sources;

import okio.Buffer;
import okio.BufferedSource;
import okio.Okio;
import okio.Source;
import okio.Timeout;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/** Decompresses an incoming source compressed with the BZip2 compression algorithm. */
public final class BZip2Source implements Source {

  private final Source source;
  private final Timeout timeout;

  /**
   * Convenient helper method that converts an input source
   * to a {@code BufferedReader} instance.
   */
  public static BufferedReader toBufferedReader(Source source) {
    BufferedSource src = Okio.buffer(new BZip2Source(source));
    BufferedReader out = new BufferedReader(new InputStreamReader(src.inputStream()));
    return out;
  }

  public BZip2Source(Source source) {
    this.timeout = source.timeout();
    this.source = bzip2Source(source);
  }

  @Override
  public long read(Buffer sink, long byteCount) throws IOException {
    return source.read(sink, byteCount);
  }

  @Override
  public Timeout timeout() {
    return timeout;
  }

  @Override
  public void close() throws IOException {
    source.close();
  }

  private Source bzip2Source(Source source) {
    Source src = null;
    try {
      src = Okio
        .source(new BZip2CompressorInputStream(Okio
                                               .buffer(source)
                                               .inputStream()));
    } catch (IOException ex) {
      throw new IllegalArgumentException("bad source");
    }
    return src;
  }
}
