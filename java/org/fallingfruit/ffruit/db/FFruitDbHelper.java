package org.fallingfruit.ffruit.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public final class FFruitDbHelper extends SQLiteOpenHelper {
  public static final int DATABASE_VERSION = 1;
  public static final String DATABASE_NAME = "ffruit.db";

  private static FFruitDbHelper instance;

  /** Returns a singleton instance of {@link FFruitDbHelper}. */
  public static synchronized FFruitDbHelper getInstance(Context context) {
    if (instance == null) {
      instance = new FFruitDbHelper(context.getApplicationContext());
    }
    return instance;
  }

  private FFruitDbHelper(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  public void onCreate(SQLiteDatabase db) {
    db.execSQL(FFruitContract.createAllTablesStmt());
  }

  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL(FFruitContract.dropAllTablesStmt());
    onCreate(db);
  }

  public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    onUpgrade(db, oldVersion, newVersion);
  }
}
