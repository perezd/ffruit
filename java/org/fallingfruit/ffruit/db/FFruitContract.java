package org.fallingfruit.ffruit.db;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;

import android.content.ContentValues;

import java.nio.charset.StandardCharsets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Helper class that defines the SQL db contract for the FFruit app.
 */
public final class FFruitContract {

  private static final String[] TABLES = new String[]{Types.TBL_NAME};

  private FFruitContract() {}

  public static interface BaseColumns extends android.provider.BaseColumns {
    public static final String COL_CHKSUM = "checksum";
  }

  public abstract static class Locations implements BaseColumns {
  }

  public abstract static class Types implements BaseColumns {

    public static final String TBL_NAME = "types";
    public static final String COL_NAME = "name";
    public static final String COL_SYNONYMS = "synonyms";
    public static final String COL_ES_NAME = "es_name";
    public static final String COL_HE_NAME = "he_name";
    public static final String COL_PT_BR_NAME = "pt_br_name";
    public static final String COL_FR_NAME = "fr_name";
    public static final String COL_DE_NAME = "de_name";
    public static final String COL_PL_NAME = "pl_name";
    public static final String COL_SCI_NAME = "scientific_name";
    public static final String COL_SCI_SYNONYMS = "scientific_synonyms";
    public static final String COL_TAXONOMIC_RANK = "taxonomic_rank";
    public static final String COL_PARENT_ID = "parent_id";
    public static final String COL_CATEGORY_MASK = "category_mask";

    private static final List<String> COLS = Arrays.asList(
      BaseColumns._ID,
      BaseColumns.COL_CHKSUM,
      COL_NAME,
      COL_SYNONYMS,
      COL_ES_NAME,
      COL_HE_NAME,
      COL_PT_BR_NAME,
      COL_FR_NAME,
      COL_DE_NAME,
      COL_PL_NAME,
      COL_SCI_NAME,
      COL_SCI_SYNONYMS,
      COL_TAXONOMIC_RANK,
      COL_PARENT_ID,
      COL_CATEGORY_MASK
    );

    public static ContentValues fromMap(Map<String, String> in) {
      return FFruitContract.fromMap(COLS, in);
    }

    public static String insertIntoStmt(List<ContentValues> vals) {
      return FFruitContract.insertIntoStmt(TBL_NAME, COLS, vals);
    }

    static String createTableStmt() {
      StringBuilder stmt = new StringBuilder();
      stmt.append(String.format("CREATE TABLE %s\n", TBL_NAME));
      stmt.append(String.format("(\n%s\n)",
                                Joiner.on(",\n")
                                .join(
                                  primaryKey(_ID),
                                  text(COL_CHKSUM),
                                  text(COL_PARENT_ID),
                                  text(COL_NAME),
                                  text(COL_SYNONYMS),
                                  text(COL_ES_NAME),
                                  text(COL_HE_NAME),
                                  text(COL_PT_BR_NAME),
                                  text(COL_FR_NAME),
                                  text(COL_DE_NAME),
                                  text(COL_PL_NAME),
                                  text(COL_SCI_NAME),
                                  text(COL_SCI_SYNONYMS),
                                  text(COL_TAXONOMIC_RANK),
                                  text(COL_CATEGORY_MASK)
                                )));
      return stmt.toString();
    }

  }

  /**
   * Given a map, return a ContentValues that mirrors it. fromMap also
   * adds an extra COL_CHKSUM entry that provides the sha256 of the map
   * values in key sorted ordered.
   *
   * <p>NOTE: This does not validate that the map keys are valid/present
   * in the target table.
   */
  public static ContentValues fromMap(Map<String, String> in) {
    ContentValues out = new ContentValues();
    Hasher sum = Hashing.sha256().newHasher();
    ArrayList<String> keys = new ArrayList<>(in.keySet());
    Collections.sort(keys);
    for (String key : keys) {
      String value = in.get(key);
      sum.putString(value, StandardCharsets.UTF_8);
      // Hack time, if we see a key named "id" we should
      // map it to the proper key.
      if (key.equals("id")) {
        key = BaseColumns._ID;
      }
      out.put(key, value);
    }
    out.put(BaseColumns.COL_CHKSUM, sum.hash().toString());
    return out;
  }

  static String createAllTablesStmt() {
    StringBuilder stmt = new StringBuilder();
    stmt.append(String.format("%s;", Types.createTableStmt()));
    return stmt.toString();
  }

  static String dropAllTablesStmt() {
    StringBuilder stmt = new StringBuilder();
    for (String tblName : TABLES) {
      stmt.append(String.format("%s;", tblName));
    }
    return stmt.toString();
  }

  private static String insertIntoStmt(String tblName,
                               List<String> cols,
                               List<ContentValues> vals) {
    // This block of code takes the list of content values
    // provided, and maps them into a list of quoted values
    // in the order defined by the provided cols.
    // Finally, it maps them into SQL-friendly format.
    // Thanks to lambdas, we can make this code very terse.
    List<String> orderedVals = Lists
      .transform(Lists.transform(vals, cv ->
                                 Lists.transform(cols, (c) ->
                                                 String.format("\"%s\"",
                                                               cv.getAsString(c)))),
        (stmt) -> String.format("(%s)", Joiner.on(", ").join(stmt)));
    return String.format("INSERT INTO %s (%s) VALUES %s;",
                         tblName,
                         Joiner.on(", ").join(cols),
                         Joiner.on(", ").join(orderedVals));
  }

  private static ContentValues fromMap(List<String> cols, Map<String, String> in) {
    for (String col : cols) {
      // Skip checksum/ID columns, those are added in for us by fromMap.
      if (col.equals(BaseColumns.COL_CHKSUM) || col.equals(BaseColumns._ID)) {
        continue;
      }
      if (!in.containsKey(col)) {
        throw new IllegalArgumentException(String.format("column '%s' missing and was expected"));
      }
    }
    return fromMap(in);
  }

  private static String dropTable(String tblName) {
    return String.format("DROP TABLE IF EXISTS %s", tblName);
  }

  private static String primaryKey(String colName) {
    return String.format("%s PRIMARY KEY", integer(colName));
  }

  private static String text(String colName) {
    return String.format("%s TEXT", colName);
  }

  private static String integer(String colName) {
    return String.format("%s INTEGER", colName);
  }

}
