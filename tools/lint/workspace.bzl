
def checkstyle():
    native.http_jar(
        name = "checkstyle",
        url = "http://iweb.dl.sourceforge.net/project/checkstyle/checkstyle/6.15/checkstyle-6.15-all.jar",
        sha256 = "cfd29fefe1abff1224ed8f57136cd88188b62164284282df5010de1c14a7d84f",
    )
