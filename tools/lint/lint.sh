#!/bin/bash



java \
    -jar external/checkstyle/jar/checkstyle-6.15-all.jar \
    -c tools/lint/java_rules.xml \
    $1
