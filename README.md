# Falling Fruit for Android -- FFruit

This Android application is a free & open source companion to the popular 
service [Falling Fruit](http://fallingfruit.org). Currently, the application 
targets Android M (Marshmallow), but I do accept patches to support older
versions of Android.

## Getting the App (From Google Play Store)

// (TODO):perezd, share official link to play store.

## Building the app yourself (From scratch)

### Prerequisites
FFruit is being developed using Linux (specifically, Ubuntu). I do not 
officially support MacOS or Windows-based development, but I am accepting 
patches to make that a reality. Feel free to contribute!

### Building from source
Once you've cloned the repo, while in the root of the project, run the
following shell script:

```
ffruit/ $ ./setup.sh
```

This should download the build system, [Bazel](bazel.io), and fetch the proper
Android SDK bits for compilation.

At the very end of the process, it will build the APK for you. Once its built, 
with your phone plugged in over USB and properly connected via `adb`, you can
run this command to load the APK onto your device:

```
ffruit/ $ ./build/bin/bazel mobile-install java/org/fallingfruit/ffruit
```

You should now see `FFruit` somewhere in your listing of apps on your device.

## Contributions are welcomed!

I encourage contributions! This is free software and anyone can fork this and 
make it better. Submit change requests, or file issues and we can all make this
application better, together! I ask that you file an Issue so that we can
discuss what you plan to contribute.

### Don't be rude
I ask that you do __not__ upload versions of this application to the 
Google Play Store (or any other app store). I will report any of these I am
notified about immediately. I will actively push updates to the
Google Play Store that encompass your changes, as soon as I review them.
